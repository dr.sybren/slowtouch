package main

import (
	"flag"
	"os"
	"time"

	"github.com/mattn/go-colorable"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var (
	flagDelay   = flag.Duration("delay", 100*time.Millisecond, "delay between files")
	flagReverse = flag.Bool("reverse", true, "reverse the list of files")
)

func main() {
	output := zerolog.ConsoleWriter{Out: colorable.NewColorableStdout(), TimeFormat: time.RFC3339}
	log.Logger = log.Output(output)

	flag.Parse()

	filenames := getFilenames()
	log.Info().
		Stringer("delay", *flagDelay).
		Int("numFiles", len(filenames)).
		Msg("slow touch")

	hadError := false
	for _, fname := range filenames {
		log.Info().Str("filename", fname).Msg("touching")

		err := touch(fname)
		if err != nil {
			log.Error().Str("filename", fname).Err(err).Msg("error touching file")
			hadError = true
		}

		time.Sleep(*flagDelay)
	}

	if hadError {
		os.Exit(1)
	}
}

// touch is a wrapper for os.Chtimes() passing 'now' as timestamp.
func touch(path string) (err error) {
	now := time.Now()
	return os.Chtimes(path, now, now)
}

func getFilenames() []string {
	var filenames []string = flag.Args()
	if *flagReverse {
		for i, j := 0, len(filenames)-1; i < j; i, j = i+1, j-1 {
			filenames[i], filenames[j] = filenames[j], filenames[i]
		}
	}
	return filenames
}
