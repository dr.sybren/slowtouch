# Slow Touch

This is a tiny CLI application that performs a filesystem 'touch' and then waits
for a little bit. A 'touch' means updating the file's modification timestamp to 'now'.

The purpose is to be able to do `slowtouch file-a file-b file-c` and get the
files' timestamps in the same chronological order as they are listed on the
commandline.

Contrary to the UNIX/Linux commandline tool `touch`, this utility will not
create new files.
